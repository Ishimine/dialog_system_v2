﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EditorPaths
{
    public static readonly string DialogTreePath = "Assets/_SO/DB/DialogTrees/";
    public static readonly string DialogPath = "Assets/_SO/DB/Dialogs/";
    public static readonly string SentencesPath = "Assets/_SO/DB/Sentences/";
    public static readonly string CharactersPath = "Assets/_SO/DB/Characters/";
    public static readonly string DatabasePath = "Assets/_SO/DB/";
    public static readonly string ManagersPath = "Assets/_SO/Managers/";

}

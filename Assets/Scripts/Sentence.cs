﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Sentence : InterpretableObject
{
    [SerializeField,ReadOnly]
    private string ownerId;
    public string OwnerId
    {
        get { return ownerId; }
        set { ownerId = value; }
    }

    private string ownerName;
    public string OwnerName
    {
        get
        {
            if(ownerName == null || ownerName =="")
            {
                ownerName = DB_Characters.Instance.GetCharacter(ownerId).CharacterName;
            }
            return ownerName;
        }
    }


    [Space(1)]

    [SerializeField]
    private string sentenceId;
    public string SentenceId
    {
        get { return sentenceId; }
        set
        {
            sentenceId = value;
        }
    }

    [Space(2)]

    [SerializeField, TextArea(3,10)]
    private string text;
    public string Text
    {
        get { return text; }
        set
        {
            text = value;
        }
    }

    [SerializeField]
    private AudioClip audio;
    public AudioClip Audio
    {
        get { return audio; }
        set
        {
            audio = value;
        }
    }

    [SerializeField]
    private float startPause;
    public float StartPause
    {
        get { return startPause; }
        set
        {
            startPause = value;
        }
    }

    [SerializeField]
    private Sprite visual;
    public Sprite Visual
    {
        get { return visual; }
        set
        {
            visual = value;
        }
    }

    public void Initialize(string sentenceId, string ownerId, string text, float startPause, Sprite visual, AudioClip audio)
    {
        this.ownerId = ownerId;
        this.sentenceId = sentenceId;
        this.text = text;
        this.startPause = startPause;
        this.visual = visual;
        this.audio = audio;
    }

    public void Initialize(string sentenceId, string ownerId, string text, float startPause, Sprite visual)
    {
        Initialize(sentenceId, ownerId, text, startPause, visual, null);
    }

    public void Initialize(string sentenceId, string ownerId, string text, float startPause, AudioClip audio)
    {
        Initialize(sentenceId, ownerId, text, startPause, null, audio);
    }

    public void Initialize(string sentenceId, string ownerId, string text, float startPause = 0)
    {
        Initialize(sentenceId, ownerId, text, startPause, null, null);
    }

    public void Initialize(string sentenceId, string ownerId, string text)
    {
        Initialize(sentenceId, ownerId, text, 0, null, null);
    }

    public void CopyTo(Sentence sentence)
    {
        sentence.Initialize(SentenceId, ownerId, text, startPause, Visual, audio);
    }

    public override Rect Draw()
    {
#if UNITY_EDITOR
        Rect rect = EditorGUILayout.BeginVertical(GUILayout.MaxWidth(400));
        Byte[] hash = UnityExtentions.StringToHashByteArray(OwnerId);
        Color orig = GUI.color;
        GUI.color = new Color((float)hash[0] / 255, (float)hash[1] / 255, (float)hash[2] / 255, .4f);
        GUI.DrawTexture(rect, EditorGUIUtility.whiteTexture);
        GUI.color = orig;

        GUILayout.BeginHorizontal();
        EditorGUILayout.SelectableLabel("ID: " + sentenceId);
        EditorGUILayout.SelectableLabel(" - " + OwnerName);
        GUILayout.EndHorizontal();
        EditorGUILayout.LabelField("    >" + text);
        GUILayout.EndVertical();
#endif
        return rect;
    }

    public override void Show(IInterpreter interpreter)
    {
#if UNITY_EDITOR
        interpreter.ShowSentence(this);
#endif
    }
}

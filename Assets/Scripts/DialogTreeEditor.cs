﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DialogTreeEditor :ScriptableObject
{
    private static DialogTreeEditor instance;
    public static DialogTreeEditor Instance
    {
        get
        {
            if (instance == null)
            {
                instance = CreateInstance<DialogTreeEditor>();
                instance = AssetDatabase.LoadAssetAtPath<DialogTreeEditor>(EditorPaths.ManagersPath + "DialogTreeEditor.asset");
                if (instance == null)
                {
                    CreateDialogTreeEditor();
                }
            }
            return instance;
        }
    }

    public string nDialogTreeName;

    private int currentTreeIndex = 0;
    public int CurrentTreeIndex
    {
        get { return currentTreeIndex; }
        set { currentTreeIndex = value; }
    }

    private static void CreateDialogTreeEditor()
    {
        instance = CreateInstance<DialogTreeEditor>();
        AssetDatabase.CreateAsset(instance, EditorPaths.ManagersPath + "DialogTreeEditor.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


    [SerializeField]
    private DialogTree currentTree;
    public DialogTree CurrentTree
    {
        get
        {
            return currentTree;
        }
    }

    [SerializeField]
    private InterpretableNode currentNode;
    public InterpretableNode CurrentNode
    {
        get
        {
            return currentNode;
        }
    }

    [SerializeField]
    private int selectedChild;

    public void SetCurrentTree(DialogTree nCurrentTree)
    {
        Debug.Log("SetCurrentTree " + nCurrentTree);
        currentTree = nCurrentTree;
        currentNode = currentTree.Root;
        selectedChild = 0;
    }

    public void SetCurrentNode(InterpretableNode nCurrentNode)
    {
        currentNode = nCurrentNode;
        selectedChild = 0;
    }

    public void GoToSelectedChild(int index)
    {
        if(index >= 0 && index < currentNode.Children.Count)
        {
            selectedChild = index;
            SetCurrentNode(currentNode.Children[selectedChild]);
        }
        else
        {
            Debug.LogWarning("Target child index is out of bounds");
        }
    }


    public void GoNext(int index)
    {
        if (currentNode == null || currentNode.Children == null || currentNode.Children.Count == 0)
        {
            Debug.Log(currentNode == null);
            Debug.Log(currentNode.Children == null);
            Debug.LogError(currentNode.Children.Count == 0);
            Debug.LogError("Cant go NEXT");
            return;
        }
        currentNode = currentNode.Children[index];
    }


    public void GoPrevious(int index)
    {
        if (currentNode == null || currentNode.Parents == null || currentNode.Parents.Count == 0)
        {
            Debug.Log(currentNode == null);
            Debug.Log(currentNode.Parents == null);
            Debug.Log(currentNode.Parents.Count == 0);
            Debug.LogError("Cant go PREVIOUS");
            return;
        }
        currentNode = currentNode.Parents[index];
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }


    public void AddChildToCurrent(InterpretableNode nChild)
    {
        Debug.Log("AddChild:" + nChild + " To Current: " + currentNode);
        if (currentNode == null)
        {
            currentNode = nChild;
            CurrentTree.Nodes = new List<InterpretableNode>();
            CurrentTree.Nodes.Add(currentNode);
            Debug.LogWarning(nChild + " is new Root");
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            return;
        }
        currentNode.AddChild(nChild);
        currentNode = nChild;
        if (!CurrentTree.Nodes.Contains(nChild)) CurrentTree.Nodes.Add(nChild);

        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    public void RemoveChildToCurrent(InterpretableNode c)
    {
        if (currentNode == null)
        {
            Debug.LogWarning("CurrentNode is NULL");
            return;
        }
        currentNode.RemoveChild(c);
        if (CurrentTree.Nodes.Contains(c)) CurrentTree.Nodes.Add(c);
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    public void AddParentToCurrent(InterpretableNode nParent)
    {
        Debug.Log("AddParent: " + nParent + " ToCurrent: " + currentNode);
        if (currentNode == null)
        {
            Debug.LogWarning("CurrentNode is NULL");
            return;
        }
        currentNode.AddParent(nParent);
        if (!CurrentTree.Nodes.Contains(nParent)) CurrentTree.Nodes.Add(nParent);
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    public void RemoveParentToCurrent(InterpretableNode c)
    {
        if (currentNode == null)
        {
            Debug.LogWarning("CurrentNode is NULL");
            return;
        }
        currentNode.RemoveParent(c);
        if (CurrentTree.Nodes.Contains(c)) CurrentTree.Nodes.Add(c);
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class SentencePack : ScriptableObject
{
    [ShowInInspector,ReadOnly]
    private string ownerId;
    public string OwnerId
    {
        get { return ownerId; }
        set { ownerId = value; }
    }

    [SerializeField, ReadOnly]
    private SentenceDictionary sentences;
    public SentenceDictionary Sentences
    {
        get
        {
            return sentences;
        }
    }

    public void Initialize(string ownerId)
    {
        this.ownerId = ownerId;
        sentences = new SentenceDictionary();
    }

    public void Initialize(string ownerId, SentenceDictionary sentences)
    {
        this.ownerId = ownerId;
        this.sentences = new SentenceDictionary();
        sentences.CopyTo(this.sentences);
    }

    [Button]
    public bool AddSentence(Sentence nSentence)
    {
        if (!sentences.ContainsKey(nSentence.SentenceId))
        {
            sentences.Add(nSentence.SentenceId, nSentence);
            Debug.Log("SentenceID: " + nSentence.SentenceId + " agregado con exito al PackID: " + nSentence.OwnerId);
            return true;
        }
        else
        {
            Debug.LogWarning("SentenceID: " + nSentence.SentenceId + " NO AGREGADO al PackID: " + nSentence.OwnerId + " ID en uso");
            return false;
        }
    }

    public bool RemoveSentence(Sentence nSentence)
    {
        if (sentences.ContainsKey(nSentence.SentenceId))
        {
            sentences.Remove(nSentence.SentenceId);
            return true;
        }
        else
            return false;
    }

    [Button]
    private void OnEnable()
    {
        if(sentences != null)
            EliminateNulls();

     /*   #if UNITY_EDITOR
        Sentence[] sentences = AssetDatabase.LoadAllAssetsAtPath();
        #endif*/
    }

    [Button]
    public void EliminateNulls()
    {
        var badKeys = sentences.Where(pair => pair.Value == null)
                        .Select(pair => pair.Key)
                        .ToList();
        foreach (var badKey in badKeys)
        {
            sentences.Remove(badKey);
        }
    }

    public void CopyTo(SentencePack sentencePack)
    {
        sentencePack.Initialize(ownerId, sentences);
    }

}
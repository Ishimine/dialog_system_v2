﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDialogInterpreter
{
    VoidEvent OnDialogStarted
    {
        get;
        set;
    }

    VoidEvent OnDialogEnded
    {
        get;
        set;
    }

    void ShowDialog(Dialog dialog);

}

public interface ISentenceInterpreter
{
    VoidEvent OnSentenceStarted
    {
        get;
        set;
    }
    VoidEvent OnSentenceEnded
    {
        get;
        set;
    }
    void ShowSentence(Sentence sentence);
}
/*
public interface IDialogSplitInterpreter
{
    DialogBrancher CurrentDialogSplit
    {
        get;
        set;
    }

    VoidEvent OnDialogSplitStarted
    {
        get;
        set;
    }

    VoidEvent OnDialogSplitEnded
    {
        get;
        set;
    }

    int SelectedOption
    {
        get;
        set;
    }
    void ShowOptions(BranchingOption[] dialogs);
    void SetOption(int selectedOption);
}





*/

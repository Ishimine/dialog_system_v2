﻿using UnityEngine;

public interface IInterpretable : IDrawable
{
    void Show(IInterpreter interpreter);
}
public interface IDrawable
{ 
    Rect Draw();
}
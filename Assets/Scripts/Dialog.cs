﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Dialog : ScriptableObject
{
    [SerializeField,ReadOnly]
    private string dialogID;
    public string DialogID
    {
        get { return dialogID; }
        set { dialogID = value; }
    }

    [SerializeField]
    private List<Sentence> sentences = new List<Sentence>();
    public List<Sentence> Sentences
    {
        get { return sentences; }
        set { sentences = value; }
    }

    [SerializeField,ReadOnly]
    private List<Character> characters;
    public List<Character> Characters
    {
        get { return characters; }
        set { characters = value; }
    }

    public void Initialize(string id, Sentence[] sentences, Character[] characters)
    {
        this.dialogID = id;
        this.sentences = new List<Sentence>(sentences);
        this.characters = new List<Character>(characters);
    }

    public void InsertSentence(Sentence sentence, int position, Character owner)
    {
        sentences.Insert(position, sentence);
    }

    public void AddSentence(Sentence sentence, Character owner)
    {
        sentences.Add(sentence);
    }

    public void RemoveSentenceAt(int position)
    {
        Sentences.RemoveAt(position);
    }

    public void RemoveSentence(Sentence sentence)
    {
        sentences.Remove(sentence);
    }

    public bool AddParticipant(Character character)
    {
        Debug.Log("character == null: " + character == null);
        if (!characters.Contains(character))
        {
            characters.Add(character);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void CopyTo(Dialog dialog)
    {
        Initialize(dialog.dialogID, dialog.sentences.ToArray(), dialog.characters.ToArray());
    }

    public void PlayWithInterpreter(IDialogInterpreter dialogInterpreter)
    {
        dialogInterpreter.ShowDialog(this);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System.IO;
using UnityEditor;
using Sirenix.OdinInspector;
using System.Linq;

[CreateAssetMenu(menuName = "DB/Sentences")]
public class DB_Sentences : ScriptableObject
{

    private static DB_Sentences instance;
    public static DB_Sentences Instance
    {
        get
        {
            if (instance == null)
            {
                instance = AssetDatabase.LoadAssetAtPath<DB_Sentences>(EditorPaths.DatabasePath + "DB_Sentences.asset");
                if (instance == null)
                {
                    CreateDB_Sentences();
                }
            }
            return instance;
        }
    }

    private readonly string packPrefix = "_Pack_";

    private int sentenceCount = 0;
    private const string sentencePrefix = "_Sent_";

    private static void CreateDB_Sentences()
    {
        instance = CreateInstance<DB_Sentences>();
        AssetDatabase.CreateAsset(instance, EditorPaths.DatabasePath + "DB_Sentences.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    [SerializeField, ReadOnly]
    private SentencePackDictionary byCharacter = new SentencePackDictionary();
    public SentencePackDictionary ByCharacter
    {
        get { return byCharacter; }
    }

    [SerializeField, ReadOnly]
    private SentenceDictionary bySentenceId = new SentenceDictionary();
    public SentenceDictionary BySentenceId
    {
        get { return bySentenceId; }
    }


    private Sentence currentSentence;
    private SentencePack currentPack;

    [Button]
    public Sentence CreateSentence(string ownerId, string text, float startPause, Sprite visual, AudioClip audioClip, string manualSentenceId = "")
    {
        string path = EditorPaths.SentencesPath + ownerId;
        CreateFolder(path);

        currentSentence = CreateInstance<Sentence>();

        string sentenceId = manualSentenceId.Replace(" ", "");
        while (bySentenceId.ContainsKey(sentenceId) || sentenceId == "")
        {
            sentenceId = sentencePrefix + sentenceCount++.ToString("00000000");
        }

        currentSentence.Initialize(sentenceId, ownerId, text, startPause, visual, audioClip);

        AddSentence(currentSentence);

        AssetDatabase.CreateAsset(currentSentence, path + "/" + currentSentence.SentenceId + ".asset");
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        return currentSentence;
    }

    public bool OverrideSentence(string sentenceId, string text, float startPause, Sprite visual, AudioClip audioClip)
    {
        if (bySentenceId.ContainsKey(sentenceId))
        {
            var s = bySentenceId[sentenceId];
            s.Initialize(sentenceId, s.OwnerId, text, startPause, visual, audioClip);
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return true;
        }
        else
            return false;
    }

    private bool AddSentence(Sentence nSentence)
    {
        ///SentenceID Dictionary
        bySentenceId.Add(nSentence.SentenceId, nSentence);

        ///CharacterId Dictionary
        if (!byCharacter.ContainsKey(nSentence.OwnerId))    ///El pack no existe
        {
            CreateSentencePack(nSentence);
        }
        else
            currentPack = byCharacter[nSentence.OwnerId];

        if (currentPack.AddSentence(nSentence))
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log("Sentence: " + nSentence.SentenceId + "  Saved.");
            return true;
        }
        else
        {
            Debug.LogWarning("Sentence: " + nSentence.SentenceId + " NOT SAVED ===> ID already exist.");
            return false;
        }
    }

    public SentencePack CreateSentencePack(string ownerId)
    {
        string path = EditorPaths.SentencesPath + ownerId + "/" + packPrefix + ownerId;
        currentPack = CreateInstance<SentencePack>();

        if (!Directory.Exists(EditorPaths.SentencesPath + ownerId))
            Directory.CreateDirectory(EditorPaths.SentencesPath + ownerId);

        AssetDatabase.CreateAsset(currentPack, path + ".asset");
        currentPack.Initialize(ownerId);
        byCharacter.Add(ownerId, currentPack);
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        return currentPack;
    }

    private SentencePack CreateSentencePack(Sentence nSentence)
    {
        string path = EditorPaths.SentencesPath + nSentence.OwnerId + "/" + packPrefix + nSentence.OwnerId;
        currentPack = CreateInstance<SentencePack>();
        AssetDatabase.CreateAsset(currentPack, path + ".asset");
        currentPack.Initialize(nSentence.OwnerId);
        byCharacter.Add(nSentence.OwnerId, currentPack);
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        return currentPack;
    }

    public bool DeleteSentence(string sentenceId)
    {
        return EliminateSentence(GetSentence(sentenceId));
    }

    [Button]
    private bool EliminateSentence(Sentence nSentence)
    {
        if (nSentence == null)
            return false;
        if (!byCharacter.ContainsKey(nSentence.OwnerId))    ///El pack no existe
        {
            Debug.LogWarning("Sentence Pack no existe");
            return false;
        }
        else
        {
            bySentenceId.Remove(nSentence.SentenceId);
            byCharacter[nSentence.OwnerId].RemoveSentence(nSentence);
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(nSentence));
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return true;
        }
    }

    public void CreateFolder(string folderPath)
    {
        folderPath = Application.dataPath.Replace("Assets", "") + folderPath;
        Debug.Log(folderPath + " exists:" + Directory.Exists(folderPath));
        if (!Directory.Exists(folderPath))
        {
            Debug.Log("Creating folder:" + folderPath);
            Directory.CreateDirectory(folderPath);
        }
    }

    [Button]
    public void PrintBySentenceID(string id)
    {
        Debug.Log(GetSentence(id).Text);
    }

    [Button]
    public void PrintByCharacterID(string id)
    {
        SentencePack sentencePack = GetSentencePack(id);

        if (sentencePack != null)
        {
            foreach (Sentence s in sentencePack.Sentences.Values)
            {
                Debug.Log(s.Text);
            }
        }
    }

    public SentencePack GetSentencePack(string characterId)
    {
        if (byCharacter.ContainsKey(characterId))
        {
            SentencePack rValue = byCharacter[characterId];
            Debug.Log("Returning " + rValue);
            return rValue;
        }
        else
        {
            Debug.LogWarning("Sentence pack NOT FOUND: " + characterId);
            return null;
        }
    }

    public Sentence GetSentence(string sentenceId)
    {
        if (bySentenceId.ContainsKey(sentenceId))
        {
            return bySentenceId[sentenceId];
        }
        else
            return null;
    }


    private void OnEnable()
    {
        EliminateNulls();

        if (byCharacter == null || byCharacter.Count == 0)
        {
            FindAllCharacterPacks();
        }

        if (bySentenceId == null || bySentenceId.Count == 0)
        {
            FindAllSentencesPacks();
        }
    }

    [Button]
    private void FindAllCharacterPacks()
    {
        byCharacter = new SentencePackDictionary();
        string[] results;
        SentencePack sentencePack;
        results = AssetDatabase.FindAssets(packPrefix);
        foreach (string guid in results)
        {
            //Debug.Log("name:test - " + );
            sentencePack = AssetDatabase.LoadAssetAtPath<SentencePack>(AssetDatabase.GUIDToAssetPath(guid));
            if (!byCharacter.ContainsKey(sentencePack.OwnerId))
            {
                byCharacter.Add(sentencePack.OwnerId, sentencePack);
            }
        }
    }

    [Button]
    private void FindAllSentencesPacks()
    {
        bySentenceId = new SentenceDictionary();
        string[] results;
        Sentence sentence;
        results = AssetDatabase.FindAssets(sentencePrefix);
        foreach (string guid in results)
        {
            //Debug.Log("name:test - " + );
            sentence = AssetDatabase.LoadAssetAtPath<Sentence>(AssetDatabase.GUIDToAssetPath(guid));
            if (!bySentenceId.ContainsKey(sentence.SentenceId))
            {
                bySentenceId.Add(sentence.SentenceId, sentence);
            }
        }
    }

    [Button]
    public void EliminateNulls()
    {
        var badKeys = bySentenceId.Where(pair => pair.Value == null)
                        .Select(pair => pair.Key)
                        .ToList();
        foreach (var badKey in badKeys)
        {
            bySentenceId.Remove(badKey);
        }

        badKeys = byCharacter.Where(pair => pair.Value == null)
                      .Select(pair => pair.Key)
                      .ToList();
        foreach (var badKey in badKeys)
        {
            byCharacter.Remove(badKey);
        }
    }

     public bool ExistSentence(string sentenceId)
    {
        return bySentenceId.ContainsKey(sentenceId);
    }


}


[System.Serializable]
public class SentenceDictionary : SerializableDictionaryBase<string, Sentence> { }

[System.Serializable]
public class SentencePackDictionary : SerializableDictionaryBase<string, SentencePack>
{
}



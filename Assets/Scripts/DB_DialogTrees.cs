﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RotaryHeart.Lib.SerializableDictionary;
using Sirenix.OdinInspector;
using System.IO;

public class DB_DialogTrees : ScriptableObject
{

    [SerializeField, ReadOnly]
    private static DB_DialogTrees instance;
    public static DB_DialogTrees Instance
    {
        get
        {
            if (instance == null)
            {
                instance = AssetDatabase.LoadAssetAtPath<DB_DialogTrees>(EditorPaths.DatabasePath + "DB_DialogTrees.asset");
                if (instance == null)
                {
                    CreateDB_Dialog();
                }
            }
            return instance;
        }
    }

    public string idPrefix = "DlgTree_";

    [SerializeField, HideInInspector]
    private int treeCount;

    [SerializeField, ReadOnly]
    private string[] keys;
    public string[] Keys
    {
        get
        {
            if (IsDirty)
            {
                UpdateIds();
            }
            return keys;
        }
        set { keys = value; }
    }

    [ShowInInspector,ReadOnly]
    private bool isDirty = true;
    public bool IsDirty
    {
        get { return isDirty; }
        set { isDirty = value; }
    }


    private void UpdateIds()
    {
        keys = new string[dialogTrees.Count];
        dialogTrees.Keys.CopyTo(keys, 0);
        IsDirty = false;
    }


    [SerializeField, ReadOnly]
    private DialogTreeDictionary dialogTrees = new DialogTreeDictionary();
    public DialogTreeDictionary DialogTrees
    {
        get { return dialogTrees; }
    }

    public bool DeleteDialog(string id)
    {
        return EliminateDialog(GetDialogTree(id));
    }

    [Button]
    private bool EliminateDialog(DialogTree nDialog)
    {
        dialogTrees.Remove(nDialog.UniqueId);
        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(nDialog));
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        return true;
    }

    private static void CreateDB_Dialog()
    {
        instance = CreateInstance<DB_DialogTrees>();
        AssetDatabase.CreateAsset(instance, EditorPaths.DatabasePath + "DB_DialogTrees.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public void CreateFolder(string folderPath)
    {
        folderPath = Application.dataPath.Replace("Assets", "") + folderPath;
        Debug.Log(folderPath + " exists:" + Directory.Exists(folderPath));
        if (!Directory.Exists(folderPath))
        {
            Debug.Log("Creating folder:" + folderPath);
            Directory.CreateDirectory(folderPath);
        }
    }

    public DialogTree GetDialogTree(string sentenceId)
    {
        if (dialogTrees.ContainsKey(sentenceId))
        {
            return dialogTrees[sentenceId];
        }
        else
            return null;
    }

    DialogTree currentDialogTree;

    [Button]
    public DialogTree CreateDialogTree(string name, bool overrideExistent, string manualId = "")
    {
        string path = EditorPaths.DialogTreePath;
        CreateFolder(path);

        currentDialogTree = CreateInstance<DialogTree>();

        string dialogId = manualId.Replace(" ", "");
        while (dialogTrees.ContainsKey(dialogId) || dialogId == "")
        {
            dialogId = idPrefix + treeCount++.ToString("00000000");
        }

        currentDialogTree.TreeName = name;
        currentDialogTree.UniqueId = dialogId;

        AddDialogTree(currentDialogTree, overrideExistent);

        AssetDatabase.CreateAsset(currentDialogTree, path + "/" + currentDialogTree.UniqueId + ".asset");
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        FindAllDialogs();
        return GetDialogTree(dialogId);
    }


    [Button]
    private bool AddDialogTree(DialogTree dialog, bool overrideIfExist)
    {
        if (!DialogTrees.ContainsKey(dialog.UniqueId))
        {
            DialogTrees.Add(dialog.UniqueId, dialog);
            IsDirty = true;
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            return true;
        }
        else if (overrideIfExist)
        {
            DialogTrees.Remove(dialog.UniqueId);
            DialogTrees.Add(dialog.UniqueId, dialog);
            IsDirty = true;
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            return true;
        }
        else
            return false;
    }


    private void OnEnable()
    {
        if (dialogTrees == null || dialogTrees.Count == 0)
        {
            FindAllDialogs();
        }
    }

    [Button]
    private void FindAllDialogs()
    {
        dialogTrees = new DialogTreeDictionary();
        string[] results;
        DialogTree dialog;
        Debug.Log(EditorPaths.DialogTreePath.Remove(EditorPaths.DialogTreePath.Length - 1, 1));
        results = AssetDatabase.FindAssets(idPrefix, new string[] { EditorPaths.DialogTreePath.Remove(EditorPaths.DialogTreePath.Length - 1, 1) });
        foreach (string guid in results)
        {
            dialog = AssetDatabase.LoadAssetAtPath<DialogTree>(AssetDatabase.GUIDToAssetPath(guid));
            if (!dialogTrees.ContainsKey(dialog.UniqueId))
            {
                dialogTrees.Add(dialog.UniqueId, dialog);
            }
        }
    }
}

[System.Serializable]
public class DialogTreeDictionary : SerializableDictionaryBase<string, DialogTree> { }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class TestInterpretable : IInterpretable
{
    [SerializeField]
    private Sentence sentence;

    public Rect Draw()
    {
        EditorGUILayout.LabelField("TestInterpretable Node");
        return GUILayoutUtility.GetLastRect();
    }

    public void Show(IInterpreter interpreter)
    {
        interpreter.ShowSentence(sentence);
    }
}
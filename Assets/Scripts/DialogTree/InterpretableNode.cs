﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class InterpretableNode : ScriptableObject, IInterpretable
{
    [SerializeField]
    private List<InterpretableNode> parents;
    public List<InterpretableNode> Parents
    {
        get
        {
            return parents;
        }
    }

    [SerializeField]
    private List<InterpretableNode> children;
    public List<InterpretableNode> Children
    {
        get
        {
            return children;
        }
    }

    [SerializeField]
    private InterpretableObject value;
    public InterpretableObject Value
    {
        get
        {
            return value;
        }
    }

    public void Show(IInterpreter interpreter)
    {
        if (value == null)
            return;
        value.Show(interpreter);
    }

    public Rect Draw()
    {
        if (value == null)
        {
            Debug.LogWarning(this + " < Cant Draw: Value is NULL");
            return new Rect();
        }
        return value.Draw();
    }

    public void Initialize(InterpretableObject value, InterpretableNode parent)
    {
        Debug.Log("<color=green> " + value +  "  </color>");
        if(value == null) Debug.LogError("InterpretableObject value IS NULL");
        this.value = value;
        this.parents = new List<InterpretableNode>();
        AddParent(parent);
        this.children = new List<InterpretableNode>();
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
#endif
    }

    public void Initialize(InterpretableObject value, List<InterpretableNode> parents)
    {
        Debug.Log("<color=green> " + value +  "  </color>");
        if(value == null) Debug.LogError("InterpretableObject value IS NULL");
        this.value = value;
        this.parents = new List<InterpretableNode>(parents);
        this.children = new List<InterpretableNode>();
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
#endif
    }

    public void AddParent(InterpretableNode nParent)
    {
        if (nParent == null) return;
        if (!parents.Contains(nParent))
        {
            parents.Add(nParent);
            nParent.AddChild(this);
        }
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
#endif
    }

    public void RemoveParent(InterpretableNode nParent)
    {
        if (nParent == null) return;
        if (parents.Contains(nParent))
        {
            parents.Remove(nParent);
            nParent.RemoveChild(this);
        }
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
#endif
    }

    public void AddChild(InterpretableNode nChild)
    {
        if (nChild == null) return;
        if (!children.Contains(nChild))
        {
            children.Add(nChild);
            nChild.AddParent(this);
        }
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
#endif
    }

    public void RemoveChild(InterpretableNode nChild)
    {
        if (nChild == null) return;
        if (children.Contains(nChild))
        {
            children.Remove(nChild);
            nChild.RemoveParent(this);
        }
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
#endif
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class InterpretableObject : ScriptableObject, IInterpretable
{
    public abstract Rect Draw();

    public abstract void Show(IInterpreter interpreter);


#if UNITY_EDITOR
    public static T CreateInstanceOfInterpretable<T>(string folder, string fileName) where T : InterpretableObject
    {
        T aux = CreateInstance<T>();
        AssetDatabase.CreateAsset(aux, folder + fileName + ".asset");
        return aux;
    }
#endif

}




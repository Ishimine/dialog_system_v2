﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Sirenix.OdinInspector;

[CreateAssetMenu]
public class DialogTree : ScriptableObject
{
    [SerializeField,ReadOnly]
    private string uniqueId;
    public string UniqueId
    {
        get { return uniqueId; }
        set { uniqueId = value; }
    }

    [SerializeField,ReadOnly]
    private string treeName;
    public string TreeName
    {
        get { return treeName; }
        set { treeName = value; }
    }

    [SerializeField]
    private List<InterpretableNode> nodes = new List<InterpretableNode>();
    public List<InterpretableNode> Nodes
    {
        get
        {
            return nodes;
        }
        set
        {
            nodes = value;
#if UNITY_EDITOR
          UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
#endif
        }
    }

    public InterpretableNode Root
    {
        get
        {
            if (nodes == null || nodes.Count == 0)
                return null;
            else
                return nodes[0];
        }
    }

    [SerializeField]
    private Dictionary<string, int> progressVariables;
    public Dictionary<string, int> ProgressVariables
    {
        get
        {
            return progressVariables;
        }
        set
        {
            progressVariables = value;
        }
    }

}
/*

public delegate void DialogEvent(Dialog value);

public abstract class DialogBrancher : IInterpretableNode
{
    [SerializeField, ReadOnly]
    protected DialogTree tree;
    public abstract IInterpretable GetTarget();
    public abstract void Show(IInterpreter interpreter);

    public void Draw()
    {
        throw new NotImplementedException();
    }

    protected int selectedTarget;

    protected DialogBrancher(DialogTree tree)
    {
        this.tree = tree;
    }

    public int SelectedTarget
    {
        get { return selectedTarget; }
        set { selectedTarget = value; }
    }
}

[System.Serializable]
public class DialogBrancher_Manual : DialogBrancher
{
    [SerializeField]
    private BranchingOption[] targetDialogs;

    public DialogBrancher_Manual(DialogTree tree, BranchingOption[] targetDialogs) : base(tree)
    {
        this.targetDialogs = targetDialogs;
        selectedTarget = -1;
    }
    
    public override IInterpretable GetTarget()
    {
        return targetDialogs[selectedTarget].targetNode;
    }

    public BranchingOption SelectDialog(int id)
    {
        SelectedTarget = id;
        return targetDialogs[SelectedTarget];
    }

    public override void Show(IInterpreter interpreter)
    {
        interpreter.ShowOptions(targetDialogs);
    }
}
[System.Serializable]
public class DialogBranching_Automatic : DialogBrancher
{
    [SerializeField]
    private DialogConditionedOption[] options;

    public DialogBranching_Automatic(DialogTree tree,DialogConditionedOption[] options) :base(tree)
    {
        this.options = options;
    }

    public override IInterpretable GetTarget()
    {
        for (int i = 0; i < options.Length; i++)
        {
            if(tree.ProgressVariables.ContainsKey(options[i].condition.Key))
            {
                if(options[i].condition.IsConditionReached(tree.ProgressVariables))
                {
                    return options[i].dialogOption.targetNode;
                }
            }
        }
        return options[UnityEngine.Random.Range(0, options.Length)].dialogOption.targetNode;
    }

    public void SelectDialog(int id)
    {
        SelectedTarget = id;
    }

    public override void Show(IInterpreter interpreter)
    {
    }
}

[System.Serializable]
public struct DialogConditionedOption
{
    public BranchingOption dialogOption;
    public KeyCondition condition;
}

[System.Serializable]
public class KeyCondition
{
    /// <summary>
    /// The key it will search for analizing the condition
    /// </summary>
    [SerializeField]
    private string key;
    public string Key
    {
        get { return key; }
        set { key = value; }
    }

    [SerializeField]
    private int keyValue;
    public int KeyValue
    {
        get { return keyValue; }
        set { keyValue = value; }
    }

    public enum ConditionType { More, MoreEqual, Less, LessEqual, Equal, NotEqual}

    [SerializeField]
    private ConditionType conditionType;

    public KeyCondition(string key, int keyValue, ConditionType conditionType)
    {
        this.conditionType = conditionType;
        this.key = key;
        this.keyValue = keyValue;
    }

    public bool IsConditionReached(IDictionary<string, int> keyValuePairs)
    {
        if (keyValuePairs.ContainsKey(key))
        {
            return IsConditionReached(keyValuePairs[key]);
        }
        else
        {
            return false;
        }
    }
    private bool IsConditionReached(int currentValue)
    {
        switch (conditionType)
        {
            case ConditionType.More:
                return currentValue > keyValue;
            case ConditionType.MoreEqual:
                return currentValue >= keyValue;
            case ConditionType.Less:
                return currentValue < keyValue;
            case ConditionType.LessEqual:
                return currentValue <= keyValue;
            case ConditionType.Equal:
                return currentValue == keyValue;
            case ConditionType.NotEqual:
                return currentValue != keyValue;
            default:
                return false;
        }
    }
}    

/*
[System.Serializable]
public struct BranchingOption
{
    public InterpretableNode targetNode;
    public string text;
    public string optionKey;
    public int optionValue;
    public Action response;

    public BranchingOption(IInterpretableNode targetNode, string text, string optionKey, int optionValue, Action response)
    {
        this.text = text;
        this.targetNode = targetNode;
        this.optionKey = optionKey;
        this.optionValue = optionValue;
        this.response = response;
    }
}

public class DialogNode : IInterpretableNode
{
    private IInterpretable targetNode;
    public IInterpretable TargetNode
    {
        get { return targetNode; }
        set { targetNode = value; }
    }

    public void Draw()
    {
    }

    public IInterpretable GetTarget()
    {
        return targetNode;
    }

    public void Show(IInterpreter interpreter)
    {
    }
}*/
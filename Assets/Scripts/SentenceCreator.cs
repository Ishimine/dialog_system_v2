﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;

public class SentenceCreator : ScriptableObject
{
    private static SentenceCreator instance;
    public static SentenceCreator Instance
    {
        get
        {
            if(instance == null)
            {
                instance = AssetDatabase.LoadAssetAtPath<SentenceCreator>(EditorPaths.ManagersPath + "SentenceCreator.asset");
                if(instance == null)
                {
                    CreateSentenceCreator();
                }
            }
            return instance;
        }
    }

    private static void CreateSentenceCreator()
    {
        instance = CreateInstance<SentenceCreator>();
        AssetDatabase.CreateAsset(instance, EditorPaths.ManagersPath + "SentenceCreator.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


    public string[] TabsNames = new string[] { "Character", "Sentence", "Dialog" };

    private int currentTabIndex;
    public int CurrentTabIndex
    {
        get
        {
            return currentTabIndex;
        }
        set
        {
            currentTabIndex = value;
        }
    }

    private string[] currentSentenceIds;
    public string[] CurrentSentenceIds
    {
        get { return currentSentenceIds; }
        set { currentSentenceIds = value; }
    }

    private int current_CharacterIndex;
    public int Current_CharacterIndex
    {
        get { return current_CharacterIndex; }
        set { current_CharacterIndex = value; }
    }


    private string currentCharacterId;
    public string CurrentCharacterId
    {
        get { return currentCharacterId; }
        set { currentCharacterId = value; }
    }

    private string currentCharacterName;
    public string CurrentCharacterName
    {
        get { return currentCharacterName; }
        set { currentCharacterName = value; }
    }

    private int current_DialogIndex;
    public int Current_DialogIndex
    {
        get { return current_DialogIndex; }
        set { current_DialogIndex = value; }
    }

    private string current_DialogId;
    public string Current_DialogId
    {
        get { return current_DialogId; }
        set { current_DialogId = value; }
    }

    private int currentSentenceIndex;
    public int CurrentSentenceIndex
    {
        get { return currentSentenceIndex; }
        set { currentSentenceIndex = value; }
    }

    [SerializeField,ReadOnly]
    private Sentence tempSentence;
    public Sentence TempSentence
    {
        get
        {
            if (tempSentence == null)
                tempSentence = CreateInstance<Sentence>();
            return tempSentence;
        }
        set
        {
            tempSentence = value;
        }
    }

    [SerializeField,ReadOnly]
    private Character tempCharacter;
    public Character TempCharacter
    {
        get
        {
            if (tempCharacter == null)
                tempCharacter = CreateInstance<Character>();
            return tempCharacter;
        }
        set
        {
            tempCharacter = value;
        }
    }

    [SerializeField,ReadOnly]
    private Dialog tempDialog;
    public Dialog TempDialog
    {
        get
        {
            if (tempDialog == null)
                tempDialog = CreateInstance<Dialog>();
            return tempDialog;
        }
        set
        {
            tempDialog = value;
        }
    }

    [SerializeField,ReadOnly]
    private SentencePack tempSentencePack;
    public SentencePack TempSentencePack
    {
        get
        {
            if (tempSentencePack == null)
                tempSentencePack = CreateInstance<SentencePack>();
            return tempSentencePack;
        }
        set
        {
            tempSentencePack = value;
        }
    }

    private GUIStyle styleTextField = new GUIStyle(EditorStyles.textArea);
    public GUIStyle StyleTextField
    {
        get
        {
            if (styleTextField == null)
                styleTextField = new GUIStyle(EditorStyles.textArea);
            return styleTextField;
        }
    }

    private GUIStyle styleTitle = new GUIStyle(EditorStyles.largeLabel);
    public GUIStyle StyleTitle
    {
        get
        {
            if (styleTitle == null)
                styleTitle = new GUIStyle(EditorStyles.textArea);
            return styleTitle;
        }
    }

    [SerializeField]
    private GUIStyle styleGlobo = new GUIStyle(EditorStyles.helpBox);
    public GUIStyle StyleGlobo
    {
        get
        {
            if (styleGlobo == null)
                styleGlobo = new GUIStyle(EditorStyles.textArea);
            return styleGlobo;
        }
    }

    private void OnEnable()
    {
        styleTitle.wordWrap = true;
        styleGlobo.wordWrap = true;
        styleTextField.wordWrap = true;
    }

    [Button]
    public void UpdateSentencesIds()
    {
        Debug.Log("UpdateSentencesIds");

        if(TempSentencePack == null)
        {
            Debug.LogError("TempSentencePack == null");
        }
        else 
        {
            CurrentSentenceIds = new string[TempSentencePack.Sentences.Count];
            TempSentencePack.Sentences.Keys.CopyTo(CurrentSentenceIds, 0);

            Debug.Log("currentSentenceIds.Length: " + CurrentSentenceIds.Length);
        }
    }


    public void LoadSentencePack(string ownerId)
    {
        DB_Sentences.Instance.GetSentencePack(ownerId).CopyTo(TempSentencePack);
    }
}

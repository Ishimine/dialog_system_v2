﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Character : ScriptableObject
{
    [SerializeField]
    private string characterName;
    public string CharacterName
    {
        get { return characterName; }
        set
        {
//            Debug.Log("Name: " + value);
            characterName = value;
        }
    }

    [SerializeField]
    private string surname;
    public string Surname
    {
        get { return surname; }
        set
        {
            surname = value;
        }
    }

    [SerializeField]
    public string FullName
    {
        get
        {
            return characterName + " " + surname;
        }
    }

    [SerializeField,ReadOnly]
    private string characterId;
    public string CharacterId
    {
        get { return characterId; }
        set {  characterId = value; }
    }


    public void Initialize(string cName, string surname, string characterId)
    {
        this.characterName = cName;
        this.surname = surname;
        this.characterId = characterId;
    }

    public void CopyTo(Character character)
    {
        character.Initialize(CharacterName, surname, characterId);
    }

}

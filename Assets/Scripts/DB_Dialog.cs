﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;
using RotaryHeart.Lib.SerializableDictionary;
using System.IO;

public class DB_Dialog : ScriptableObject
{
    [SerializeField, ReadOnly]
    private static DB_Dialog instance;
    public static DB_Dialog Instance
    {
        get
        {
            if (instance == null)
            {
                instance = AssetDatabase.LoadAssetAtPath<DB_Dialog>(EditorPaths.DatabasePath + "DB_Dialog.asset");
                if (instance == null)
                {
                    CreateDB_Dialog();
                }
            }
            return instance;
        }
    }

    public string idPrefix = "Dlg_";

    [SerializeField,HideInInspector]
    private int dialogCount;

    [SerializeField, ReadOnly]
    private string[] keys;
    public string[] Keys
    {
        get
        {
            if(IsDirty)
            {
                UpdateIds();
            }
            return keys;
        }
        set { keys = value; }
    }

    private bool isDirty = true;
    public bool IsDirty
    {
        get { return isDirty; }
        set { isDirty = value; }
    }


    private void UpdateIds()
    {
        keys = new string[dialogs.Count];
        dialogs.Keys.CopyTo(keys, 0);
        IsDirty = false;
    }

    [SerializeField, ReadOnly]
    private DialogDictionary dialogs = new DialogDictionary();
    public DialogDictionary Dialogs
    {
        get { return dialogs; }
    }

    public bool DeleteDialog(string id)
    {
       return EliminateDialog(GetDialog(id));
    }

    [Button]
    private bool EliminateDialog(Dialog nDialog)
    {
        dialogs.Remove(nDialog.DialogID);
        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(nDialog));
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        return true;
    }

    private static void CreateDB_Dialog()
    {
        instance = CreateInstance<DB_Dialog>();
        AssetDatabase.CreateAsset(instance, EditorPaths.DatabasePath + "DB_Dialog.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public void CreateFolder(string folderPath)
    {
        folderPath = Application.dataPath.Replace("Assets", "") + folderPath;
        Debug.Log(folderPath + " exists:" + Directory.Exists(folderPath));
        if (!Directory.Exists(folderPath))
        {
            Debug.Log("Creating folder:" + folderPath);
            Directory.CreateDirectory(folderPath);
        }
    }

    public Dialog GetDialog(string sentenceId)
    {
        if (dialogs.ContainsKey(sentenceId))
        {
            return dialogs[sentenceId];
        }
        else
            return null;
    }


    Dialog currentDialog;

    [Button]
    public Dialog CreateDialog(Sentence[] sentences, Character[] participants, string manualDialogId = "", bool overrideExistence = false)
    {
        currentDialog = CreateInstance<Dialog>();

        string dialogId = manualDialogId.Replace(" ", "");
        while (dialogs.ContainsKey(dialogId) || dialogId == "")
        {
            dialogId = idPrefix + dialogCount++.ToString("00000000");
        }

        currentDialog.Initialize(dialogId, sentences, participants);


        string path = EditorPaths.DialogPath;
        CreateFolder(path);

        AddDialog(currentDialog, overrideExistence);

        AssetDatabase.CreateAsset(currentDialog, path + "/" + currentDialog.DialogID + ".asset");
        EditorUtility.SetDirty(this);
        EditorUtility.SetDirty(currentDialog);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        return currentDialog;
    }


    [Button]
    private bool AddDialog(Dialog dialog, bool overrideIfExist)
    {
        if (!Dialogs.ContainsKey(dialog.DialogID))
        {
            Dialogs.Add(dialog.DialogID, dialog);
            IsDirty = true;
            return true;
        }
        else if (overrideIfExist)
        {
            Dialogs.Remove(dialog.DialogID);
            Dialogs.Add(dialog.DialogID, dialog);
            IsDirty = true;
            return true;
        }
        else
            return false;
    }

    private void OnEnable()
    {
        if (dialogs == null || dialogs.Count == 0)
        {
            FindAllDialogs();
        }
    }

    [Button]
    private void FindAllDialogs()
    {
        dialogs = new DialogDictionary();
        string[] results;
        Dialog dialog;
        Debug.Log(EditorPaths.DialogPath.Remove(EditorPaths.DialogPath.Length - 1, 1));
        results = AssetDatabase.FindAssets(idPrefix, new string[] { EditorPaths.DialogPath.Remove(EditorPaths.DialogPath.Length - 1, 1) });
        foreach (string guid in results)
        {
            dialog = AssetDatabase.LoadAssetAtPath<Dialog>(AssetDatabase.GUIDToAssetPath(guid));
            if (!dialogs.ContainsKey(dialog.DialogID))
            {
                dialogs.Add(dialog.DialogID, dialog);
            }
        }
    }
}

[System.Serializable]
public class DialogDictionary : SerializableDictionaryBase<string, Dialog> { }

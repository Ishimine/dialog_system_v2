﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using Sirenix.OdinInspector;
using UnityEditor;
using System.Linq;
using System.IO;

[CreateAssetMenu(menuName = "DB/Character")]
public class DB_Characters : ScriptableObject
{
    private static DB_Characters instance;
    public static DB_Characters Instance
    {
        get
        {
            if (instance == null)
            {
                instance = AssetDatabase.LoadAssetAtPath<DB_Characters>(EditorPaths.DatabasePath + "DB_Characters.asset");
                if (instance == null)
                {
                    CreateDB_Characters();
                }
            }
            return instance;
        }
    }

    private static void CreateDB_Characters()
    {
        instance = CreateInstance<DB_Characters>();
        AssetDatabase.CreateAsset(instance, EditorPaths.DatabasePath + "DB_Characters.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


    [SerializeField, ReadOnly]
    private CharacterDictionary characters = new CharacterDictionary();
    public CharacterDictionary Characters
    {
        get { return characters; }
    }

    private static string prefix = "_Char_";

    [SerializeField,ReadOnly]
    private string[] keys;
    public string[] Keys
    {
        get
        {
            if(IsDirty)
            {
                UpdateArrays();
            }
            return keys;
        }
    }

    [SerializeField,ReadOnly]
    private string[] characterNames;
    public string[] CharacterNames
    {
        get
        {
            if (IsDirty)
            {
                UpdateArrays();
            }
            return characterNames;
        }
    }

    [SerializeField,ReadOnly]
    private bool isDirty = true;
    public bool IsDirty
    {
        get { return isDirty; }
        set { isDirty = value; }
    }

    [SerializeField,ReadOnly]
    private int currentCount = 0;
    Character currentCharacter;

    [SerializeField,HideInInspector]
    private Character[] charactersArray;
    public Character[] CharactersArray 
    {
        get
        {
            if(IsDirty)
            {
                UpdateArrays();
            }
            return charactersArray;
        }
    }

    [Button]    
    public Character CreateCharacter(string name, string surname)
    {
        currentCharacter = CreateInstance<Character>();
        currentCharacter.Initialize(name, surname, GetNewCharacterId());

        string path = EditorPaths.CharactersPath + prefix + currentCharacter.FullName;
        string fPath = path;
        int n = -1;
        while (File.Exists(fPath))
        {
            n++;
            fPath = path + n.ToString();
        }

        AddCharacter(currentCharacter, false);
        DB_Sentences.Instance.CreateSentencePack(currentCharacter.CharacterId);

        AssetDatabase.CreateAsset(currentCharacter, path + ".asset");
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        return currentCharacter;
    }

    public Character GetCharacter(string characterId)
    {
        if (!characters.ContainsKey(characterId))
            return null;
        return characters[characterId];
    }

    private string GetNewCharacterId()
    {
        string id;
        do
        {
            id = prefix + currentCount++.ToString("00000000");

        } while (characters.ContainsKey(id));
        return id;
    }

    public bool OverrideCharacter(string id, string name, string surname)
    {
        if (characters.ContainsKey(id))
        {
            Character c = characters[id];
            c.Initialize(name,surname,id);
            IsDirty = true;
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return true;
        }
        else
        {
            return false;
        }
    }

    [Button]
    private bool AddCharacter(Character character, bool overrideIfExist)
    {
        Debug.Log("characterId: " + character.CharacterId);
        if (!characters.ContainsKey(character.CharacterId))
        {
            characters.Add(character.CharacterId, character);
            IsDirty = true;
            return true;
        }
        else if (overrideIfExist)
        {
            characters.Remove(character.CharacterId);
            characters.Add(character.CharacterId, character);
            IsDirty = true;
            return true;
        }
        else
            return false;
    }

    public bool DeleteCharacter(string characterId)
    {
        if(characters.ContainsKey(characterId))
                return DeleteCharacter(characters[characterId]);
        else    return false;
    }

    [Button]
    private bool DeleteCharacter(Character character)
    {
        if (characters.ContainsKey(character.CharacterId))
        {
            characters.Remove(character.CharacterId);
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(character));
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            IsDirty = true;
            return true;
        }
        else return false;
    }

    [Button]
    private void UpdateArrays()
    {
        UpdateKeys();
        UpdateCharacterNames();
        IsDirty = false;
    }

    private void UpdateKeys()
    {
        keys = new string[characters.Count];
        characters.Keys.CopyTo(keys,0);
    }

    private void UpdateCharacterNames()
    {
        charactersArray = characters.Values.ToArray();
        characterNames = new string[charactersArray.Length];
        for (int i = 0; i < charactersArray.Length; i++)
        {
            characterNames[i] = charactersArray[i].FullName;
        }
    }

    [Button]
    public void EliminateNulls()
    {
        var badKeys = characters.Where(pair => pair.Value == null)
                        .Select(pair => pair.Key)
                        .ToList();
        foreach (var badKey in badKeys)
        {
            characters.Remove(badKey);
        }
    }

    private void OnEnable()
    {
        if(characters == null || characters.Count == 0)
        {
            FindAllCharacters();
        }
    }

    [Button]
    private void FindAllCharacters()
    {
        characters = new CharacterDictionary();
        string[] results;
        Character character;
        Debug.Log(EditorPaths.CharactersPath.Remove(EditorPaths.CharactersPath.Length - 1, 1));
        results = AssetDatabase.FindAssets(prefix,new string[] { EditorPaths.CharactersPath.Remove(EditorPaths.CharactersPath.Length - 1, 1)});
        foreach (string guid in results)
        {
            character = AssetDatabase.LoadAssetAtPath<Character>(AssetDatabase.GUIDToAssetPath(guid));
            if (!characters.ContainsKey(character.CharacterId))
            {
                characters.Add(character.CharacterId, character);
            }
        }
    }
}

[System.Serializable]
public class CharacterDictionary : SerializableDictionaryBase<string, Character> { }

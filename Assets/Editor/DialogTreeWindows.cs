﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DialogTreeWindows : EditorWindow
{
    private static DialogTreeWindows instance;
    public static DialogTreeWindows Instance
    {
        get
        {
            return instance;
        }
    }

    private DialogTreeEditor Target
    {
        get
        {
            return DialogTreeEditor.Instance;
        }
    }

    [MenuItem("Window/DialogTreeWindows")]
    public static void ShowWindow()
    {
        GetWindow<DialogTreeWindows>("DialogTreeWindows");
    }

    public DialogTreeWindows()
    {
        instance = this;
    }

    private void OnEnable()
    {
        Target.CurrentTreeIndex = -1;
    }

    private int selectedChild;
    private int selectedParent;

    private void OnGUI()
    {
        if (Target.CurrentTree != null)
        {
            DrawDialogTreeSelection();
            if (Target.CurrentTree.Root == null)
            {
                Debug.Log("A");
                DrawNodeCreation();
            }
            else
            {
//                Debug.Log("B");
                GUILayout.Label("Tree Name:" + Target.CurrentTree.TreeName);
                GUILayout.Space(20);

                GUILayout.BeginVertical();
                GUILayout.Label("PARENTS");
                GUILayout.BeginHorizontal();
                GUILayout.Space(20);
                Rect currentR;
                if (Target.CurrentNode != null && Target.CurrentNode.Parents.Count > 0)
                {
                    for (int i = 0; i < Target.CurrentNode.Parents.Count; i++)
                    {
                        GUILayout.BeginHorizontal();

                        if (selectedParent == i) EditorGUILayout.LabelField(">", GUILayout.MaxWidth(10));
                        GUILayout.BeginVertical();
                        currentR = Target.CurrentNode.Parents[i].Draw();
                        if (GUILayout.Button("↑", GUILayout.MaxWidth(400)))
                        {
                            Target.SetCurrentNode(Target.CurrentNode.Parents[i]);
                            break;
                        }
                        GUILayout.EndVertical();
                        GUILayout.EndHorizontal();
                    }
                }

                GUILayout.EndHorizontal();
                GUILayout.EndVertical();

                GUILayout.Space(10);
                GUILayout.BeginVertical();
                EditorGUILayout.LabelField("     ↓↓↓↓");
                GUILayout.Label("     ↓↓↓↓");
                GUILayout.Label("     ↓↓↓↓");
                GUILayout.Label("   ↓↓↓↓↓↓↓↓");
                GUILayout.Label("    ↓↓↓↓↓↓");
                GUILayout.Label("     ↓↓↓↓");
                GUILayout.Label("      ↓↓");
                GUILayout.EndVertical();
                GUILayout.Space(10);


                GUILayout.BeginVertical();
                GUILayout.Label("Current");
                GUILayout.BeginHorizontal();

                if (Target.CurrentNode != null)
                    Target.CurrentNode.Draw();
                else
                    Target.SetCurrentNode(Target.CurrentTree.Root);

                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.BeginHorizontal();
                GUILayout.BeginVertical();
                GUILayout.Label("     ↓↓↓↓");
                GUILayout.Label("     ↓↓↓↓");
                GUILayout.Label("     ↓↓↓↓");
                GUILayout.Label("   ↓↓↓↓↓↓↓↓");
                GUILayout.Label("    ↓↓↓↓↓↓");
                GUILayout.Label("     ↓↓↓↓");
                GUILayout.Label("      ↓↓");
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
                GUILayout.Space(10);
                GUILayout.EndVertical();

                GUILayout.BeginVertical();
                GUILayout.Label("CHILDS");
                GUILayout.BeginHorizontal();
                GUILayout.Space(20);

                if (Target.CurrentNode != null && Target.CurrentNode.Children.Count > 0)
                {
                    for (int i = 0; i < Target.CurrentNode.Children.Count; i++)
                    {
                        GUILayout.BeginHorizontal();
                        if (selectedChild == i) EditorGUILayout.LabelField(">", GUILayout.MaxWidth(20));
                        GUILayout.BeginVertical();
                        Target.CurrentNode.Children[i].Draw();
                        if (GUILayout.Button("↓",GUILayout.MaxWidth(400)))
                        {
                            Target.SetCurrentNode(Target.CurrentNode.Children[i]);
                            break;
                        }
                        GUILayout.EndVertical();
                        GUILayout.EndHorizontal();
                    }
                }
                GUILayout.EndHorizontal();
                GUILayout.EndVertical();

                GUILayout.Space(10);
                GUILayout.BeginHorizontal();

                if (GUILayout.Button("Next") && Target.CurrentNode.Children.Count >= 1)
                {
                    Target.SetCurrentNode(Target.CurrentNode.Children[0]);
                }
                if(GUILayout.Button("Previous") && Target.CurrentNode.Parents.Count >= 1)
                {
                    Target.SetCurrentNode(Target.CurrentNode.Parents[0]);
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginVertical();

                GUILayout.Space(10);
                DrawNodeCreation();
            }
        }
        else if (DB_DialogTrees.Instance.DialogTrees.Count > 0)
        {
            Debug.Log("C");
            DrawDialogTreeSelection();
        }
        else
        {
            Debug.Log("D");
            GUILayout.BeginHorizontal();
            GUILayout.Label("Name:");
            Target.nDialogTreeName = EditorGUILayout.TextField(Target.nDialogTreeName, GUILayout.MaxWidth(float.MaxValue));
            GUILayout.EndHorizontal();
            if (GUILayout.Button("Create New Dialog Tree"))
            {
                if (Target.nDialogTreeName.Replace(" ", "") == "")
                    return;

                Target.CurrentTreeIndex = 0;
                Target.SetCurrentTree(DB_DialogTrees.Instance.CreateDialogTree(Target.nDialogTreeName, false, ""));
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
        HandleKeyboard();
    }

    private void DrawDialogTreeSelection()
    {
        int selection = EditorGUILayout.Popup("DialogTree: ", Target.CurrentTreeIndex, DB_DialogTrees.Instance.Keys);
        if (Target.CurrentTreeIndex != selection)
        {
            Target.CurrentTreeIndex = selection;
            Target.SetCurrentTree(DB_DialogTrees.Instance.GetDialogTree(DB_DialogTrees.Instance.Keys[selection]));
        }
        else if (Target.CurrentTree == null)
        {
            Target.SetCurrentTree(DB_DialogTrees.Instance.GetDialogTree(DB_DialogTrees.Instance.Keys[0]));
        }
    }

    private void DrawNodeCreation()
    {
        GUILayout.BeginHorizontal();

        if(GUILayout.Button("Create Sentence Node"))
        {
            SentenceNodeWindow.ShowWindow();
        }
        else if(GUILayout.Button("Create Options Node"))
        {

        }

        GUILayout.EndHorizontal();
    }

    private void HandleKeyboard()
    {
        Event current = Event.current;
        if (current.type != EventType.KeyDown)
            return;
        switch (current.keyCode)
        {
            case KeyCode.N:
                SentenceNodeWindow.ShowWindow();
                Debug.Log("N");
                break;
            case KeyCode.UpArrow:
                Debug.Log("UpArrow");
                Debug.Log("Target.CurrentNode.Parents.Count : " + Target.CurrentNode.Parents.Count);
                Debug.Log("selectedParent: " + selectedParent);
                if (Target.CurrentNode.Parents.Count > 0)
                {
                    Target.SetCurrentNode(Target.CurrentNode.Parents[Mathf.Clamp(selectedParent, 0, Target.CurrentNode.Parents.Count - 1)]);
                    Repaint();
                }
                break;
            case KeyCode.DownArrow:
                Debug.Log("DownArrow");
                Debug.Log("Target.CurrentNode.Children.Count : " + Target.CurrentNode.Children.Count);
                Debug.Log("selectedChild: " + selectedChild);
                if (Target.CurrentNode.Children.Count > 0)
                {
                    Target.SetCurrentNode(Target.CurrentNode.Children[Mathf.Clamp(selectedChild, 0, Target.CurrentNode.Children.Count - 1)]);
                    Repaint();
                }
                break;
            case KeyCode.LeftArrow:
                Debug.Log("LeftArrow");
                selectedParent = Mathf.Clamp(selectedParent - 1, 0, Target.CurrentNode.Parents.Count - 1);
                selectedChild = Mathf.Clamp(selectedChild - 1, 0, Target.CurrentNode.Children.Count - 1);
                Debug.Log("selectedParent: " + selectedParent);
                Debug.Log("selectedChild: " + selectedChild);
                Repaint();
                break;
            case KeyCode.RightArrow:
                Debug.Log("RightArrow");
                selectedParent = Mathf.Clamp(selectedParent + 1, 0, Target.CurrentNode.Parents.Count - 1);
                selectedChild = Mathf.Clamp(selectedChild + 1, 0, Target.CurrentNode.Children.Count - 1);
                Debug.Log("selectedParent: " + selectedParent);
                Debug.Log("selectedChild: " + selectedChild);
                Repaint();
                break;
            case KeyCode.Return:
                Debug.Log("Return");
                break;
            case KeyCode.KeypadEnter:
                Debug.Log("KeypadEnter");
                break;
            case KeyCode.Backspace:
                Debug.Log("Backspace");
                break;
            case KeyCode.Escape:
                Debug.Log("Escape");
                break;
        }
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;
using System.Text;
using System.Security.Cryptography;

public class SentenceCreatorWindow : EditorWindow
{   
    private SentenceCreator Target
    {
        get
        {
            return SentenceCreator.Instance;
        }
    }

    [MenuItem("Window/SentenceCreator")]
    public static void ShowWindow()
    {
        GetWindow<SentenceCreatorWindow>("SentenceCreator");
    }

    static private float height;

    static Vector2 scrolPosition;

    private void OnGUI()
    {
        height = 0;

        Target.CurrentTabIndex = GUILayout.Toolbar(Target.CurrentTabIndex, Target.TabsNames);

        scrolPosition= GUILayout.BeginScrollView(scrolPosition);
        GUILayout.BeginVertical();
        GUILayout.Space(15);

        switch (Target.CurrentTabIndex)
        {
            case 0:
                DrawCharacterEditionSection();
                break;
            case 1:
                DrawSentenceEditionSection();
                break;
            case 2:
                DrawDialogEditionSection();
                break;
            default:
                break;
        }

        GUILayout.EndVertical();
        GUILayout.EndScrollView();
    }


    private void DrawDialogEditionSection()
    {
        GUILayout.Label("DialogEdition");

        DrawDialogSelection();

        GUILayout.Space(20);

        if (Target.TempDialog != null)
        {
            for (int i = 0; i < Target.TempDialog.Sentences.Count; i++)
            {
                GUILayout.Space(5);

                if(Target.TempDialog.Sentences[i] == null)
                {
                    Debug.LogWarning("Target.TempDialog.Sentences["+i+ "] IS null");
                    continue;
                }
                if ( DrawSentenceFromDialogEditBox(Target.TempDialog.Sentences[i], i, i % 2 == 0))
                {
                    break;
                }
                GUILayout.Space(5);
            }

            GUILayout.BeginHorizontal();
            DrawCharacterSelection();
            if (GUILayout.Button("+", GUILayout.Width(30), GUILayout.Height(30)))
            {
                Target.TempDialog.AddParticipant(Target.TempCharacter);
            }
            GUILayout.EndHorizontal();
            Target.TempCharacter = null;

            GUILayout.Space(15);

            DrawNewSentenceForDialog();

            GUILayout.Space(15);


            /*  DrawCharacterSelection();
              if (tempCharacter != null)
              {
                  DrawSentenceEdition();
              }

              if (tempCharacter != null)
              {
                  GUILayout.Space(15);
                  DrawSentenceSelection();

                if (tempSentence != null)
                  {
                      GUILayout.Space(10);
                      GUILayout.BeginHorizontal();

                      GUIStyle style = new GUIStyle(EditorStyles.textArea);
                      style.wordWrap = true;
                      GUILayout.Label(tempSentence.Text, style);

                      GUILayout.BeginHorizontal(GUILayout.Width(30), GUILayout.Height(30));
                      if (GUILayout.Button("+", GUILayout.Width(30), GUILayout.Height(30)))
                      {
                          currentDialog.Load(tempSentence, tempCharacter);
                      }
                      GUILayout.EndHorizontal();

                      GUILayout.EndHorizontal();
                  }
              }*/
        }

        DrawDialogSaveAndLoad();
    }

    private void DrawCharacterEditionSection()
    {
        DrawCharacterSelection();
        DrawCharacterEdition();
        DrawCharacterSaveAndLoad();
    }

    private void DrawNewSentenceForDialog()
    {
        GUILayout.BeginHorizontal();
        DrawSentenceEdition();
        if (GUILayout.Button("+", GUILayout.Width(30), GUILayout.Height(30)))
        {
            Sentence s;
            if (!DB_Sentences.Instance.ExistSentence(Target.TempSentence.SentenceId))
            {
                s = DB_Sentences.Instance.CreateSentence(Target.TempSentence.OwnerId, Target.TempSentence.Text, Target.TempSentence.StartPause, Target.TempSentence.Visual, Target.TempSentence.Audio, Target.TempSentence.SentenceId);
                LoadSentencePack(Target.CurrentCharacterId);
            }
            else
            {
                s = DB_Sentences.Instance.GetSentence(Target.TempSentence.SentenceId);
                if (s.Text != Target.TempSentence.Text)
                {
                    s = DB_Sentences.Instance.CreateSentence(Target.TempSentence.OwnerId, Target.TempSentence.Text, Target.TempSentence.StartPause, Target.TempSentence.Visual, Target.TempSentence.Audio, Target.TempSentence.SentenceId);
                    LoadSentencePack(Target.CurrentCharacterId);
                }
            }
            Target.TempDialog.AddSentence(s, Target.TempCharacter);
        }
        GUILayout.EndHorizontal();
    }

    private void DrawSentenceEditionSection()
    {
        if(DrawCharacterSelection() && Target.CurrentTabIndex == 3)
        {
            Target.TempDialog.AddParticipant(Target.TempCharacter);
        }

        if (Target.CurrentCharacterName != null)
        {
            DrawSentenceSelection();
            DrawSentenceEdition();
            DrawSentenceSaveAndLoad();
        }
    }

    private void DrawDialogSelection()
    {
        if (DB_Dialog.Instance.Keys.Length <= 0)
        {
            if (GUILayout.Button("NEW"))
            {
                Target.TempDialog = CreateInstance<Dialog>();
                DB_Dialog.Instance.CreateDialog(new Sentence[0], new Character[0]);
            }
            return;
        }

        int selection = EditorGUILayout.Popup(Target.Current_DialogIndex, DB_Dialog.Instance.Keys, GUILayout.Height(13));
        if (Target.Current_DialogIndex != selection)
        {
            Target.Current_DialogIndex = selection;
            Target.Current_DialogId = DB_Dialog.Instance.Keys[Target.Current_DialogIndex];
            LoadDialog(Target.Current_DialogId);
        }
    }

    private bool DrawCharacterSelection()
    {
        EditorGUILayout.BeginHorizontal();
        int selection = EditorGUILayout.Popup("CharacterFullName: ", Target.Current_CharacterIndex, DB_Characters.Instance.CharacterNames, GUILayout.Height(10));
        if(Target.Current_CharacterIndex != selection || Target.TempCharacter == null)
        {
            Target.Current_CharacterIndex = selection;
            LoadCharacter(selection);
            LoadSentencePack(Target.CurrentCharacterId);

            if (Target.TempSentencePack.Sentences.Count > 0)
                LoadSentence(Target.TempSentencePack.Sentences.Keys.ToArray()[0]);
            else
                Target.TempSentence.Initialize("", Target.TempCharacter.CharacterId, "New text...");

            GUILayout.Label("ID: " + Target.CurrentCharacterId);
            return true;
        }
        GUILayout.Label("ID: " + Target.CurrentCharacterId);
        EditorGUILayout.EndHorizontal();
        return false;
    }

    private void DrawSentenceSaveAndLoad()
    {
        GUILayout.Space(20);
        bool reset = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save New"))
        {
            SaveCurrentSentence();
            reset = true;
            LoadSentencePack(Target.TempCharacter.CharacterId);
        }
        if (GUILayout.Button("Override"))
        {
            OverrideSentence();
            reset = true;
            LoadSentencePack(Target.TempCharacter.CharacterId);
        }
        if (GUILayout.Button("Delete"))
        {
            DB_Sentences.Instance.DeleteSentence(Target.TempSentence.SentenceId);
            LoadSentencePack(Target.TempCharacter.CharacterId);
            reset = true;
        }
        if (reset)
        {
            Target.CurrentSentenceIndex = 0;
        }

        GUILayout.EndHorizontal();
    }

    private void DrawCharacterSaveAndLoad()
    {
        GUILayout.Space(20);
        bool reset = false;
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        if(GUILayout.Button("Save New"))
        {
            SaveCharacter();
            LoadCharacter(Target.TempCharacter.CharacterId);
            reset = true;
        }
        if(GUILayout.Button("Override"))
        {
            OverrideCharacter();
            LoadCharacter(Target.TempCharacter.CharacterId);
            reset = true;
        }
        if (GUILayout.Button("Delete"))
        {
            DB_Characters.Instance.DeleteCharacter(Target.TempCharacter.CharacterId);
            if(DB_Characters.Instance.Characters.Count > 0)
                LoadCharacter(0);
            reset = true;
        }

        if (reset)
        {
            Target.CurrentCharacterId = "";
            Target.CurrentCharacterName = "";
        }
     
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
    }

    private void DrawDialogSaveAndLoad()
    {
        GUILayout.Space(20);
        GUILayout.Label("DIALOG SAVE");

        GUILayout.Space(20);
        bool reset = false;
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save New"))
        {
            SaveDialog();
            LoadDialog(Target.Current_DialogId);
            reset = true;
        }
        if (GUILayout.Button("Override"))
        {
            OverrideDialog();
            LoadDialog(Target.Current_DialogId);
            reset = true;
        }
        if (GUILayout.Button("Delete"))
        {
            DB_Dialog.Instance.DeleteDialog(Target.TempDialog.DialogID);
            if(DB_Dialog.Instance.Dialogs.Count > 0)
                LoadDialog(Target.Current_DialogId);
            reset = true;
        }

        if (reset)
        {
            Target.CurrentCharacterId = "";
            Target.CurrentCharacterName = "";
        }

        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
    }

    /// <summary>
    /// Retorna si algun boton fue tocado lo que implica un "BREAK"
    /// </summary>
    /// <param name="sentence"></param>
    /// <param name="index"></param>
    /// <param name="invert"></param>
    /// <returns></returns>
    private bool DrawSentenceFromDialogEditBox(Sentence sentence, int index, bool invert = false)
    {

        invert = false;
        GUILayout.BeginHorizontal();

        Character character = DB_Characters.Instance.GetCharacter(sentence.OwnerId);

        Rect rect = EditorGUILayout.BeginHorizontal();

        Byte[] hash = UnityExtentions.StringToHashByteArray(sentence.OwnerId);
        Color orig = GUI.color;
        GUI.color = new Color((float)hash[0] / 255, (float)hash[1] / 255, (float)hash[2] / 255,.4f);
        GUI.DrawTexture(rect, EditorGUIUtility.whiteTexture);
        GUI.color = orig;

        if (!invert) GUILayout.Label(character.FullName, Target.StyleTitle, GUILayout.Width(100));
        GUILayout.Label(sentence.Text, Target.StyleGlobo);
        if (invert) GUILayout.Label(character.FullName, Target.StyleTitle, GUILayout.Width(100));
        EditorGUILayout.EndHorizontal();

       EditorGUILayout.BeginHorizontal(GUILayout.Width(60), GUILayout.Height(30));


  

        if (index != 0)
        {
            if (GUILayout.Button("↑", GUILayout.Width(30), GUILayout.Height(30)))
            {
                Target.TempDialog.Sentences.Swap(index, index - 1);
                return true;
            }
        }
        else GUILayout.Button("↑", GUILayout.Width(30), GUILayout.Height(30));

        if (index < Target.TempDialog.Sentences.Count - 1)
        {
            if (GUILayout.Button("↓", GUILayout.Width(30), GUILayout.Height(30)))
            {
                Target.TempDialog.Sentences.Swap(index, index + 1);
            return true;
            }
        }
        else GUILayout.Button("↓", GUILayout.Width(30), GUILayout.Height(30));

        if (GUILayout.Button("X", GUILayout.Width(30), GUILayout.Height(30)))
        {
            Target.TempDialog.Sentences.RemoveAt(index);
            return true;
        }

        if (GUILayout.Button("Edit", GUILayout.Width(30), GUILayout.Height(30)))
        {
            //currentDialog.Sentences.Remove(sentence);
            LoadSentencePack(sentence.OwnerId);
            LoadSentence(sentence.SentenceId);
            return true;
        }

        EditorGUILayout.EndHorizontal();  

        //GUILayout.Label(DB_Characters.Instance.GetCharacter(sentence.OwnerId).FullName);
        GUILayout.EndHorizontal();
        return false;
    }


  

    private void LoadCharacter(int index)
    {
        Target.CurrentCharacterId = DB_Characters.Instance.Keys[index];
        Target.CurrentCharacterName = DB_Characters.Instance.CharacterNames[index];
        DB_Characters.Instance.Characters[Target.CurrentCharacterId].CopyTo(Target.TempCharacter);
    }

    private void LoadCharacter(string id)
    {
        DB_Characters.Instance.Characters[id].CopyTo(Target.TempCharacter);
        Target.CurrentCharacterId = Target.TempCharacter.CharacterId;
        Target.CurrentCharacterName = Target.TempCharacter.FullName;
    }

    private void LoadSentencePack(string characterId)
    {
        Debug.Log("LoadSentencePack: " + characterId);
        if (Target.CurrentCharacterName != "")
        {
            Debug.Log("True");
            DB_Sentences.Instance.GetSentencePack(Target.CurrentCharacterId).CopyTo(Target.TempSentencePack);
            Target.UpdateSentencesIds();
        }
        else
            Debug.Log("False");
    }

    private Sentence LoadSentence(string id)
    {
        Debug.Log("LoadSentence: " + id);
        Target.TempSentencePack.Sentences[id].CopyTo(Target.TempSentence);
        Target.UpdateSentencesIds();
        return Target.TempSentence;
    }

    private void LoadDialog(string dialogId)
    {
        DB_Dialog.Instance.GetDialog(dialogId).CopyTo(Target.TempDialog);
    }

    private void SaveCurrentSentence()
    {
        Sentence s = DB_Sentences.Instance.CreateSentence(Target.TempCharacter.CharacterId, Target.TempSentence.Text, Target.TempSentence.StartPause, Target.TempSentence.Visual, Target.TempSentence.Audio, Target.TempSentence.SentenceId.Replace(" ",""));
        LoadSentencePack(Target.TempSentence.OwnerId);
        LoadSentence(s.SentenceId);
    }

    private void OverrideSentence()
    {
        DB_Sentences.Instance.OverrideSentence(Target.TempSentence.SentenceId, Target.TempSentence.Text, Target.TempSentence.StartPause, Target.TempSentence.Visual, Target.TempSentence.Audio);
    }


    private void OverrideCharacter()
    {
        if(!DB_Characters.Instance.OverrideCharacter(Target.CurrentCharacterId, Target.TempCharacter.CharacterName, Target.TempCharacter.Surname))
        {
            SaveCharacter();
        }
    }
        
    private void SaveCharacter()
    {
        DB_Characters.Instance.CreateCharacter(Target.TempCharacter.CharacterName, Target.TempCharacter.Surname).CopyTo(Target.TempCharacter);
         //DB_Sentences.Instance.CreateSentence(c.CharacterId, "New text...", 0, null, null, c.CharacterId + "_NewSentence");
    }

    private void SaveDialog()
    {
        Debug.Log("TempDialog: " + Target.TempDialog);
        DB_Dialog.Instance.CreateDialog(Target.TempDialog.Sentences.ToArray(), Target.TempDialog.Characters.ToArray(), Target.TempDialog.DialogID, false);
    }

    private void OverrideDialog()
    {
        DB_Dialog.Instance.CreateDialog(Target.TempDialog.Sentences.ToArray(), Target.TempDialog.Characters.ToArray(), Target.TempDialog.DialogID, true);
    }

    private void LoadDialog()
    {
        DB_Dialog.Instance.GetDialog(Target.TempDialog.DialogID).CopyTo(Target.TempDialog);
    }


    private void DrawCharacterEdition()
    {
        EditorGUILayout.BeginVertical();

        GUILayout.Label("Name:");
        Target.TempCharacter.CharacterName = EditorGUILayout.TextField(Target.TempCharacter.CharacterName, Target.StyleTextField, GUILayout.MaxWidth(float.MaxValue) );
        GUILayout.Label("Surname:");
        Target.TempCharacter.Surname = EditorGUILayout.TextField(Target.TempCharacter.Surname, Target.StyleTextField, GUILayout.MaxWidth(float.MaxValue));

        EditorGUILayout.EndVertical();

    }

    private bool DrawSentenceSelection()
    {
       if(Target.TempSentencePack != null && Target.TempSentencePack.Sentences.Count > 0)
        {
            int selection = EditorGUILayout.Popup("SentenceID: ", Target.CurrentSentenceIndex, Target.CurrentSentenceIds );
            if (Target.CurrentSentenceIndex != selection)
            {
                Target.CurrentSentenceIndex = selection;
                LoadSentence(Target.CurrentSentenceIds[selection]);
                return true;
            }
        }
        return false;
    }

    private void DrawSentenceEdition()
    {
        float imgSize = Mathf.Clamp( Mathf.Min(Screen.width, Screen.height) / 2, 65, 120);

        GUILayout.BeginVertical();

        GUILayout.Space(10);
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Sentence ID:");
        Target.TempSentence.SentenceId = EditorGUILayout.TextField(Target.TempSentence.SentenceId, GUILayout.MaxWidth(float.MaxValue));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(15);

        EditorGUILayout.BeginHorizontal();
        Target.TempSentence.Visual = (Sprite)EditorGUILayout.ObjectField(Target.TempSentence.Visual, typeof(Sprite), false, GUILayout.Height(imgSize), GUILayout.Width(imgSize));
        GUILayout.Label("Text: ");
        Target.TempSentence.Text = EditorGUILayout.TextField(Target.TempSentence.Text, Target.StyleTextField, GUILayout.Height(imgSize), GUILayout.MaxWidth(float.MaxValue));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        Target.TempSentence.Audio = (AudioClip)EditorGUILayout.ObjectField(Target.TempSentence.Audio, typeof(AudioClip), false, GUILayout.Width(imgSize));
        GUILayout.Label("WaitTime:");
        Target.TempSentence.StartPause = EditorGUILayout.FloatField(Target.TempSentence.StartPause, GUILayout.MaxWidth(float.MaxValue));
        GUILayout.Space(10);
        EditorGUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }
}

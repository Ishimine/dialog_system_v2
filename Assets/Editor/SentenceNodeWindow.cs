﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;


public class SentenceNodeWindow : EditorWindow
{
    //[MenuItem("Window/DialogTreeWindows")]
    public static void ShowWindow()
    {
        GetWindow<SentenceNodeWindow>("SentenceNodeWindow");

    }

    private SentenceCreator Target
    {
        get
        {
            return SentenceCreator.Instance;
        }
    }

    private void OnEnable()
    {
        Focus();
    }

    private bool quiting = false;
    private bool saving = false;


    private void OnGUI()
    {
        DrawCharacterSelection();

        DrawSentenceEdition();

        HandleKeyboard();
    }

    private bool DrawCharacterSelection()
    {
        EditorGUILayout.BeginHorizontal();
        int selection = EditorGUILayout.Popup("CharacterFullName: ", Target.Current_CharacterIndex, DB_Characters.Instance.CharacterNames, GUILayout.Height(10));
        if (Target.Current_CharacterIndex != selection || Target.TempCharacter == null)
        {
            Target.Current_CharacterIndex = selection;
            LoadCharacter(selection);
            LoadSentencePack(Target.CurrentCharacterId);

            if (Target.TempSentencePack.Sentences.Count > 0)
                LoadSentence(Target.TempSentencePack.Sentences.Keys.ToArray()[0]);
            else
                Target.TempSentence.Initialize("", Target.TempCharacter.CharacterId, "New text...");

            GUILayout.Label("ID: " + Target.CurrentCharacterId);
            return true;
        }
        GUILayout.Label("ID: " + Target.CurrentCharacterId);
        EditorGUILayout.EndHorizontal();
        return false;
    }

    private void DrawSentenceEdition()
    {
        float imgSize = Mathf.Clamp(Mathf.Min(Screen.width, Screen.height) / 2, 65, 120);

        GUILayout.BeginVertical();

        GUILayout.Space(10);
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Sentence ID:");
        Target.TempSentence.SentenceId = EditorGUILayout.TextField(Target.TempSentence.SentenceId, GUILayout.MaxWidth(float.MaxValue));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(15);

        EditorGUILayout.BeginHorizontal();
        Target.TempSentence.Visual = (Sprite)EditorGUILayout.ObjectField(Target.TempSentence.Visual, typeof(Sprite), false, GUILayout.Height(imgSize), GUILayout.Width(imgSize));
        GUILayout.Label("Text: ");
        Target.TempSentence.Text = EditorGUILayout.TextField(Target.TempSentence.Text, Target.StyleTextField, GUILayout.Height(imgSize), GUILayout.MaxWidth(float.MaxValue));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        Target.TempSentence.Audio = (AudioClip)EditorGUILayout.ObjectField(Target.TempSentence.Audio, typeof(AudioClip), false, GUILayout.Width(imgSize));
        GUILayout.Label("WaitTime:");
        Target.TempSentence.StartPause = EditorGUILayout.FloatField(Target.TempSentence.StartPause, GUILayout.MaxWidth(float.MaxValue));
        GUILayout.Space(10);
        EditorGUILayout.EndHorizontal();

        GUILayout.EndVertical();



        if(quiting)
        {
            if (EditorUtility.DisplayDialog("Closing window", "You are clossing without saving, are you sure?", "Close", "Cancel"))
            {
                Close();
                DialogTreeWindows.Instance.Focus();
            }
            else
            {
                quiting = false;
            }
        }
        else if(saving)
        {
            if (EditorUtility.DisplayDialog("Saving sentence", "Saving sentence  \n " + Target.CurrentCharacterName +" \n " + Target.TempSentence.Text, "Save", "Cancel"))
            {
                Sentence sentence = SaveCurrentSentence();

                InterpretableNode nNode = CreateInstance<InterpretableNode>();
                nNode.Initialize(sentence, DialogTreeEditor.Instance.CurrentNode);
                AssetDatabase.CreateAsset(nNode, EditorPaths.DialogTreePath + "_SNode_" + sentence.SentenceId + ".asset");
                DialogTreeEditor.Instance.AddChildToCurrent(nNode);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                Close();
                DialogTreeWindows.Instance.Focus();
            }
            else
            {
                saving = false;
            }
        }
    }

    private Sentence SaveCurrentSentence()
    {
        Sentence s = DB_Sentences.Instance.CreateSentence(Target.TempCharacter.CharacterId, Target.TempSentence.Text, Target.TempSentence.StartPause, Target.TempSentence.Visual, Target.TempSentence.Audio, Target.TempSentence.SentenceId.Replace(" ", ""));
        LoadSentencePack(Target.TempSentence.OwnerId);
        LoadSentence(s.SentenceId);
        return s;
    }

    private void LoadCharacter(int index)
    {
        Target.CurrentCharacterId = DB_Characters.Instance.Keys[index];
        Target.CurrentCharacterName = DB_Characters.Instance.CharacterNames[index];
        DB_Characters.Instance.Characters[Target.CurrentCharacterId].CopyTo(Target.TempCharacter);
    }

    private void LoadCharacter(string id)
    {
        DB_Characters.Instance.Characters[id].CopyTo(Target.TempCharacter);
        Target.CurrentCharacterId = Target.TempCharacter.CharacterId;
        Target.CurrentCharacterName = Target.TempCharacter.FullName;
    }

    private void LoadSentencePack(string characterId)
    {
        Debug.Log("LoadSentencePack: " + characterId);
        if (Target.CurrentCharacterName != "")
        {
            Debug.Log("True");
            DB_Sentences.Instance.GetSentencePack(Target.CurrentCharacterId).CopyTo(Target.TempSentencePack);
            Target.UpdateSentencesIds();
        }
        else
            Debug.Log("False");
    }

    private Sentence LoadSentence(string id)
    {
        Debug.Log("LoadSentence: " + id);
        Target.TempSentencePack.Sentences[id].CopyTo(Target.TempSentence);
        Target.UpdateSentencesIds();
        return Target.TempSentence;
    }


    private void HandleKeyboard()
    {
        Event current = Event.current;
        if (current.type != EventType.KeyDown)
            return;
        switch (current.keyCode)
        {
            case KeyCode.Escape:
                Debug.Log("Escape");
                quiting = true;
                break;
            case KeyCode.KeypadEnter:
                saving = true;
                Debug.Log("KeypadEnter");
                break;
            case KeyCode.LeftArrow:
                Debug.Log("LeftArrow");
                break;
            case KeyCode.RightArrow:
                Debug.Log("RightArrow");
                break;
            case KeyCode.Return:
                saving = true;
                Debug.Log("Return");
                break;
            case KeyCode.Backspace:
                Debug.Log("Backspace");
                break;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTreeInterpreter_Test0 : MonoBehaviour, IInterpreter
{
    private VoidEvent onDialogStarted;
    public VoidEvent OnDialogStarted            { get { return onDialogStarted; } set { onDialogStarted = value; } }
    private VoidEvent onDialogEnded;
    public VoidEvent OnDialogEnded              { get => onDialogEnded; set => onDialogEnded = value; }
    private VoidEvent onSentenceStarted;
    public VoidEvent OnSentenceStarted          { get => onSentenceStarted; set => onSentenceStarted = value; }
    private VoidEvent onSentenceEnded;
    public VoidEvent OnSentenceEnded            { get => onSentenceEnded; set => onSentenceEnded = value; }
   // private DialogBrancher currentDialogSplit;
   // public DialogBrancher CurrentDialogSplit       { get => currentDialogSplit; set => currentDialogSplit = value; }
    private VoidEvent onDialogSplitStarted;
    public VoidEvent OnDialogSplitStarted       { get => onDialogSplitStarted; set => onDialogSplitStarted = value; }
    private VoidEvent onDialogSplitEnded;
    public VoidEvent OnDialogSplitEnded         { get => onDialogSplitEnded; set => onDialogSplitEnded = value; }
    private int selectedOption;
    public int SelectedOption                   { get => selectedOption; set => selectedOption = value; }

    private CodeAnimator CodeAnimator = new CodeAnimator();

    private IInterpretable nextInterpretable;
    public IInterpretable NextInterpretable
    {
        get { return nextInterpretable; }
        set { nextInterpretable = value; }
    }

    public void Next()
    {
        nextInterpretable.Show(this);
    }

    public void Previus()
    {
    }

    public void SetOption(int selectedOption)
    {
        throw new System.NotImplementedException();
    }

    public void ShowDialog(Dialog dialog)
    {
        throw new System.NotImplementedException();
    }

    public void ShowSentence(Sentence sentence)
    {
        throw new System.NotImplementedException();
    }

 /*   public void ShowSentence(DialogBrancher dialog)
    {
        throw new System.NotImplementedException();
    }

    public void ShowOptions(BranchingOption[] dialogs)
    {
        throw new System.NotImplementedException();
    }*/

    ///Hacer el dialog Interpreter como prueba para poder hacercarme mas a la utilizacion real del systema. Diagramar flujo
}
